#' Select and -if needed- unzip genome from f
#' 
#' @description 
#' Given the common abbreviated name/id of a genome
#' Identifies genome file from folder with multiple gzip/FASTA files
#' The file corresponding to the genome sequence of that species
#' Unzip it
#' Return the unzipped file name
#' 
#' @param gencode The common abbreviated name/id of a genome.
#' 
#' Depends on the package 'R.utils'.
#' @import R.utils
#' 
#' @return Name of the FASTA file with the genome sequence
#' 
#' @author Adria Sisternes Domene \email{adriasisternes@@gmail.com}
#' 
#' @export selectgenome

selectgenome <- function(gencode){
  dir <- "annotation/motif"
  gen.gz <- paste(gencode, "_genome.fa.gz", sep = "")
  out <- gsub(".gz", "", gen.gz)
  path <- file.path(dir, gen.gz)
  if(file.exists(file.path(dir, out))){
    message(paste("Warning: ", gen.gz, " seems to be already decompressed in folder.", ""))
    message("Skipping.")
  } else {
    message(paste("Unzipping ", gen.gz, sep = ""))
    gunzip(path, remove = FALSE)
  }
  return(out)
}