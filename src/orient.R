#' Generate BED files with motif orientation
#' 
#' @description
#' From a directory,
#' Read Fimo Output Formatted as tab-based .txt file
#' Produce BED-formatted file with motif orientation
#' 
#' @param dir A directory with the output of FIMO. 
#' @seealso \code{\link{fimo.R}} to run FIMO on input seqs/motifs.
#' 
#' Depends on the package 'bedr'. The function 'get.fasta' is used.
#' @import bedr 
#' 
#' @return A BED-formatted file with regions generated from motif orientation.
#' 
#' @author Adria Sisternes Domene \email{adriasisternes@@gmail.com}
#' 
#' @export orient

orient <- function(dir){
  filenames <- list.files(dir,
                          pattern = "*.txt",
                          recursive = TRUE,
                          full.names = TRUE)
  message("Reading file named: ")
  for(i in 1:length(filenames)){
    file <- filenames[i]
    shortname <- basename(file)
    message(shortname)
    describer = gsub(".fimo.txt", "", shortname)
    dir.create(path = paste("bin/tmp/", describer, sep=""))
    output_filename = paste("bin/tmp/", describer,
                            "/", describer, "_orient.bed", 
                            sep = "")
    fimo_out <- read.delim(file)
    sorted <- fimo_out[order(fimo_out$score, decreasing = TRUE),]
    message(paste(nrow(sorted), "matches found."))
    duplicates_removed <- sorted[!duplicated(sorted$matched_sequence),]
    message("Keeping match with highest score per sequence...")
    message(paste(nrow(duplicates_removed), "matches left."))
    bed_output <- duplicates_removed[,c(1:3,5,8)]
    sorted_bed_output <- as.data.frame(bedr.sort.region(bed_output))
    emptycol <- data.frame(V1=NA)
    formated_output <- cbind(sorted_bed_output$seqnames,
                             sorted_bed_output$start,
                             sorted_bed_output$end,
                             emptycol$V1,
                             sorted_bed_output$score,
                             sorted_bed_output$strand,
                             emptycol$V1,
                             emptycol$V1,
                             emptycol$V1,
                             emptycol$V1)
    write.table( x = formated_output,
                 file = output_filename,
                 sep="\t", col.names=FALSE, row.names=FALSE, quote=FALSE )
  }
}