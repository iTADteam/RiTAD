#!/bin/bash -l

#$ -N iTADprediction
#$ -m e
#$ -pe omp 4

module load bedtools

##########################

# Check if TAD file is being used. If not, then use a genome-appropriate dummy file
tad=$2
tad=${2:-$1'_dummy_tads.bed'}

#ensure that the intra-TAD loops contain at least 1 TSS
bedtools intersect -wa -u -a ../04_mergedloops/CTCF/merged_cac_loops.bed12 -b ../annotation/TSS/$1*.bed > ../05_filteredloops/CTCF/merged_cac_loops_tss.bed12
bedtools intersect -wa -u -a ../04_mergedloops/Coh/merged_cac_loops.bed12 -b ../annotation/TSS/$1*.bed > ../05_filteredloops/Coh/merged_cac_loops_tss.bed12

#ensure that the intra-TAD loops do not substantially overlap TADs
bedtools intersect -wa -v -a ../05_filteredloops/CTCF/merged_cac_loops_tss.bed12 -b '../annotation/TADs/'$tad -f 0.8 -r > ../05_filteredloops/CTCF/merged_cac_loops_tss_tads.bed12
bedtools intersect -wa -v -a ../05_filteredloops/Coh/merged_cac_loops_tss.bed12 -b '../annotation/TADs/'$tad -f 0.8 -r > ../05_filteredloops/Coh/merged_cac_loops_tss_tads.bed12

echo 'Prior to filtering the number of merged loops is:'
echo ' '
wc -l ../04_mergedloops/CTCF/*cac*.bed12
echo ' '
echo 'After filtering for TSS overlap the number of merged loops is:'
echo ' '
wc -l ../05_filteredloops/CTCF/merged_cac_loops_tss.bed12
echo ' '
echo 'After filtering for TAD loop overlap the FINAL number of merged loops is:'
echo ' '
wc -l ../05_filteredloops/CTCF/merged_cac_loops_tss_tads.bed12

#Moving Bed12 files that can be uploaded to the UCSC genome browser
cp ../05_filteredloops/CTCF/merged_cac_loops_tss_tads.bed12 ../05_filteredloops/Final_Output/final_iTADs_ctcf_score.bed12
cp ../05_filteredloops/Coh/merged_cac_loops_tss_tads.bed12 ../05_filteredloops/Final_Output/final_iTADs_coh_score.bed12

#processing to reformat the loop output to a format that can be uploaded to the WashU Genome Browser
cut -f 4-5 ../05_filteredloops/Final_Output/final_iTADs_coh_score.bed12 | sed 's:_:\t:g' | cut -f 1-3 > ../05_filteredloops/Final_Output/final_iTADs_ctcf_washu.txt
cut -f 4-5 ../05_filteredloops/Final_Output/final_iTADs_coh_score.bed12 | sed 's:_:\t:g' | cut -f 1-2,4 > ../05_filteredloops/Final_Output/final_iTADs_coh_washu.txt

#processing to extract intra-TAD loop anchors for any downstream comparative analyses of loop anchors vs other CTCF/CAC
cut -f 4-5 ../05_filteredloops/Final_Output/final_iTADs_coh_score.bed12 | sed 's:_:\t:g' | sed 's/:/\t/g' | 
sed 's:-:\t:g' | cut -f 1-3 | bedtools sort -i stdin | bedtools merge -i stdin | bedtools sort -i stdin > tmp/tmp1.tx
cut -f 4-5 ../05_filteredloops/Final_Output/final_iTADs_coh_score.bed12 | sed 's:_:\t:g' | sed 's/:/\t/g' |
sed 's:-:\t:g' | cut -f 4-6 | bedtools sort -i stdin | bedtools merge -i stdin | bedtools sort -i stdin > tmp/tmp2.tx
cat tmp/tmp1.tx tmp/tmp2.tx | 
bedtools sort -i stdin > ../05_filteredloops/Final_Output/final_iTAD_loop_anchors.bed

rm tmp/*.tx