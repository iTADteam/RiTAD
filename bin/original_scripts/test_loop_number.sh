#!/bin/bash -l

#$ -N SE_m
#$ -m e

module load bedtools
module load samtools

# USAGE: ./test_loop_number.sh <"mm9" or "hg19">

if [ $# -ne 1 ]
then
	echo "Usage: ./test_loop_number.sh <Genome>"
	echo "<Genome> = Needs to be either mm9 or hg19"
	exit 
fi

genom=$1

while IFS='' read -r line || [[ -n "$line" ]]; do
    ./predict_ctcf_chipseq_loops.sh $line $genom
done < percentages.txt

wait

# Generate File Labels
for f in tmp/CTCF/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 echo $bed
done >> tmp/ctcf_names.tx
for f in tmp/Coh/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 echo $bed
done >> tmp/coh_names.tx

#Generate Counts
for f in tmp/CTCF/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 wc -l $f
done >> tmp/ctcf_counts.tx
#
for f in tmp/Coh/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 wc -l $f
done >> tmp/coh_counts.tx

# Generate Overlaps
for f in tmp/CTCF/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 bedtools intersect -wa -u -f 0.8 -r -a $f -b ../../annotation/ChIA-PET/$genom*.bed | wc -l
done >> tmp/ctcf_overlaps.tx
#
for f in tmp/Coh/*.bed12
do
 bed=`basename $f | sed 's/.bed12//'`
 bedtools intersect -wa -u -f 0.8 -r -a $f -b ../../annotation/ChIA-PET/$genom*.bed | wc -l
done >> tmp/coh_overlaps.tx

paste tmp/ctcf_names.tx tmp/ctcf_counts.tx tmp/ctcf_overlaps.tx | sed 's: :\t:g' | cut -f 1,2,4 | 
awk 'BEGIN { OFS = "\t" } { $4 = $3 / $2 } 1' | sort -k 4 -n -r | sed 's:_prop:_prop\t:g' | 
sed 's:_inputanchors_prop::g' > tmp/ctcf_table.tx

paste tmp/coh_names.tx tmp/coh_counts.tx tmp/coh_overlaps.tx | sed 's: :\t:g' | cut -f 1,2,4 | 
awk 'BEGIN { OFS = "\t" } { $4 = $3 / $2 } 1' | sort -k 4 -n -r | sed 's:_prop:_prop\t:g' | 
sed 's:_inputanchors_prop::g' > tmp/coh_table.tx

echo 'Name	Prop Used	Loop Number	Loops Overlapping	Fraction of Loops' |
cat - tmp/ctcf_table.tx > output/ctcf_table.txt

echo 'Name	Prop Used	Loop Number	Loops Overlapping	Fraction of Loops' |
cat - tmp/coh_table.tx > output/coh_table.txt

wait

rm tmp/*.tx
rm tmp/C*/*.bed12
