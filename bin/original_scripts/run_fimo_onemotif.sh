#!/bin/bash -l

#$ -N runfimoonemotif
#$ -m e
#$ -pe omp 4

module load meme/4.10.0_4
module load bedtools

#run for mouse genome: /annotation/motif/mm9_genome.fa.gz
#run for human genome: /annotation/motif/hg19_genome.fa.gz
#run for other genome: /annotation/motif/{name}_genome.fa.gz

echo $1

gunzip '../annotation/motif/'$1'_genome.fa.gz'

wait

for inputbed in ../01_inputbeds/CTCF/*.bed;
do
describer=`basename $inputbed | sed 's/.bed//'`

echo $inputbed
echo $describer 

bedtools getfasta -fi '../annotation/motif/'$1'_genome.fa' -bed '../01_inputbeds/CTCF/'$describer'.bed' > tmp/$describer'.fa' 

wait

fimo --o tmp/$describer --motif MA0139.1 ../annotation/motif/all.meme tmp/$describer'.fa'

wait

tail -n +2 'tmp/'$describer'/fimo.txt' | sort -rnk6 | awk -F"\t" '!x[$2]++' | cut -f2-6 | awk '{gsub(":", "\t", $0); print}' | 
awk '{gsub("-", "\t", $2); print}' | sed 's: :\t:g' | bedtools sort -i stdin > 'tmp/'$describer'/'$describer'_orient.bed'

tail -n +2 'tmp/'$describer'/fimo.txt' | sort -rnk6 | awk -F"\t" '!x[$2]++' | cut -f2-6 | awk '{gsub(":", "\t", $0); print}' | 
awk '{gsub("-", "\t", $2); print}' | sed 's: :\t:g' | bedtools sort -i stdin | awk 'BEGIN { OFS = "\t" } { $8 = $2 + $4 } 1' | 
awk 'BEGIN { OFS = "\t" } { $9 = $2 + $5 } 1' | awk 'BEGIN { OFS = "\t" } { print $1, $8, $9, NR, $7, $6 }' > 'tmp/'$describer'/'$describer'_motifpos.bed'

bedtools intersect -wa -wb -a '../01_inputbeds/CTCF/'$describer'.bed' -b 'tmp/'$describer'/'$describer'_orient.bed' | 
sort -k 5 -n | awk 'BEGIN { OFS = "\t" } { print $1, $2, $3, NR, $5 * ($7 / 10), $16 }' | bedtools sort -i stdin > 'tmp/'$describer'/'$describer'_inputanchors.bed'

cp 'tmp/'$describer'/'$describer'_inputanchors.bed' ../02_inputanchors/CTCF

done

wait

for inputbed in ../01_inputbeds/Coh/*.bed;
do
describer=`basename $inputbed | sed 's/.bed//'`

echo $inputbed
echo $describer

bedtools getfasta -fi '../annotation/motif/'$1'_genome.fa' -bed '../01_inputbeds/Coh/'$describer'.bed' > tmp/$describer'.fa' 

wait

fimo --o tmp/$describer --motif MA0139.1 ../annotation/motif/all.meme tmp/$describer'.fa'

wait

tail -n +2 'tmp/'$describer'/fimo.txt' | sort -rnk6 | awk -F"\t" '!x[$2]++' | cut -f2-6 | awk '{gsub(":", "\t", $0); print}' | 
awk '{gsub("-", "\t", $2); print}' | sed 's: :\t:g' | bedtools sort -i stdin > 'tmp/'$describer'/'$describer'_orient.bed'

tail -n +2 'tmp/'$describer'/fimo.txt' | sort -rnk6 | awk -F"\t" '!x[$2]++' | cut -f2-6 | awk '{gsub(":", "\t", $0); print}' | 
awk '{gsub("-", "\t", $2); print}' | sed 's: :\t:g' | bedtools sort -i stdin | awk 'BEGIN { OFS = "\t" } { $8 = $2 + $4 } 1' | 
awk 'BEGIN { OFS = "\t" } { $9 = $2 + $5 } 1' | awk 'BEGIN { OFS = "\t" } { print $1, $8, $9, NR, $7, $6 }' > 'tmp/'$describer'/'$describer'_motifpos.bed'

bedtools intersect -wa -wb -a '../01_inputbeds/Coh/'$describer'.bed' -b 'tmp/'$describer'/'$describer'_orient.bed' | 
sort -k 5 -n | awk 'BEGIN { OFS = "\t" } { print $1, $2, $3, NR, $5 * ($7 / 10), $16 }' | bedtools sort -i stdin > 'tmp/'$describer'/'$describer'_inputanchors.bed'

cp 'tmp/'$describer'/'$describer'_inputanchors.bed' ../02_inputanchors/Coh

done

wait

gzip ../02_inputanchors/*/*.bed

gzip '../annotation/motif/'$1'_genome.fa'
