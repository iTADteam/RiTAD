#!/bin/bash -l

#$ -N iTADprediction
#$ -m e
#$ -pe omp 4

# RUN WITH OPTIONS:
## $1: genome build, either for mouse ("mm9") or human ("hg19"). This is used for motif
### finding with meme, first to extract the sequence within peaks then search those 
### sequences for the CTCF motif. This is also used to determine which TSS file to use for
### filtering during filtering. Additional genomes can be used if the gzip compressed 
### fasta file (*.fa.gz) and index (*.fai) are stored in the annotation directory: 
### iTAD_loopprediction/annotation/motif using the naming convention {name}_genome.fa.gz.
### Then you would use the {name} as input in the first field when running ./runpipe.sh.
#
## $2: Fraction of peaks to filter during loop prediction, you can use the script 
### ./test_loop_number.sh to determine a suitable fraction to use. Default is 0.4 or
### or keeping the top 40% of peaks.
#
## $3: Target pre-filtering loop count target, recommended range is 20,000 to 30,000 loops
### depending on the number of replicates and overlap between them. Default is 20,000
#
## $4: TAD file for filtering, used in 04_filtered loop prediction. As placed in the
### directory annotation/TADs/*.bed with the full *.bed filename passed as a variable

# EXAMPLE USAGE: 
# ./run_pipe.sh mm9 0.40 20000 liver_mm9_tads.bed
# This would run the pipeline to predict loops for all peak files in 01_inputbeds/CTCF and
# 01_inputbeds/Coh. It assumes that the these files are for the mm9 genome, will keep the
# top 40% of peaks and 20,000 loops before merging replicates. The merged loops will be
# filtered based on mm9 TSS coordinates and liver TAD coordinates (also mm9).

# NECESSARY INPUTS:
## For mouse (mm9) and human (hg19) the only inputs necessary are two bed files, one for
## CTCF and one for cohesin. The score column is expected to be standard output from
## MACS2, although any consistent scoring schema should work. Genomes must be downloaded
## and stored as either 'mm9_genome.fa.gz' or 'hg19_genome.fa.gz' in the directory
## annotation/motif and indexed to generate an accompanying *.fai file
##
## For other genome assemblies, the user must provide (1) a fasta file for motif discovery
## (2) a TSS file for TSS overlap and (3) a TAD file from either the tissue/cell type used
## or at least the same genome assembly (optional, this filter will be ignored if no TAD
## coordinates are provided.

# PIPELINE OUTPUTS (Found at 05_filteredloops/Final_Output):
#(1) Merged and filtered intra-TAD loops in bed12 format (for UCSC visualization)
#(2) Merged and filtered intra-TAD loops in WashU browser format (for WashU visualization)
#(3) Anchors for intra-TAD loops (for downstream analyses)

genom=$1
fract=$2
loop=$3
tad=$4

#if parameters aren't set, then use default parameters, below
genom=${1:-'mm9'}
fract=${2:-'0.40'}
loop=${3:-'25000'}

echo '***Testing Options***'
echo ' '
echo 'If no options were set when running ./run_pipe.sh then default parameters are used'
echo ' '
echo 'Genome Build is:'
echo $genom
echo ' '
echo 'Fraction of loops is:'
echo $fract
echo ' '
echo 'Target loop number is:'
echo $loop
echo ' '
echo 'TAD filtering is:'
if [ -z $tad ]; then echo "not being used"; else echo "done using $tad"; fi

cd bin

# Starts with input peak files in O1_inputbeds/ directory, split into CTCF/ and Coh/
# subdirectories. Outputs anchor inputs for predicting intra-TAD loops in the next stage.
# This does not include any pre-filtering for # of replicates, CAC only, or IDR filtering
# which is left up to the end user to define what constitutes a "reproducible peak".

./run_fimo_onemotif.sh $genom

wait

# Starts with input anchor files in 02_inputanchors/ directory, split into CTCF/ and Coh/
# subdirectories. Outputs unfiltered loops in directory /03_loops and merged loops in
# directory /04_mergedloops.

./predict_ctcf_chipseq_loops.sh $fract $loop

wait

# Starts with input filter loop files in 04_mergedloops/ directory, split into CTCF/ 
# and Coh/ subdirectories.

./filter_loops.sh $genom $tad
