#!/bin/bash -l 

module load bedtools
module load python

##################################################################################################
##################################################################################################
## Predict CTCF loops and domains based on motif orientation in CTCF ChIP-seq peaks.
##################################################################################################
##################################################################################################

# Predict CTCF loops for CTCF ChIP-seq data (using peak proportions)
for f in ../../02_inputanchors/CTCF/*.bed.gz
do
 echo "Predicting CTCF loops based on peak motif orientations for $f"
 bed=`basename $f | sed 's/.bed.gz//'`
 python2 ctcf_peaks2loops_2.py -p 1.0:$1:0.01 -n ${bed} -i $f -o tmp/CTCF/${bed}_prop$1.bed12
done

wait

for f in ../../02_inputanchors/Coh/*.bed.gz
do
 echo "Predicting CTCF loops based on peak motif orientations for $f"
 bed=`basename $f | sed 's/.bed.gz//'`
 python2 ctcf_peaks2loops_2.py -p 1.0:$1:0.01 -n ${bed} -i $f -o tmp/Coh/${bed}_prop$1.bed12
done
