#An overview of three tools provided in association with Matthews and Waxman 2018
# Please see the scripts described below for more detailed parameter descriptions

#For intra-TAD loop prediction, use ./runpipe.sh with additional documentation in the header
Input:  (1) Bed files for CTCF placed in 01_inputbeds/CTCF/*.bed and for Cohesin placed in 
		01_inputbeds/Coh/*.bed. These are typically directly from Macs2 output (rename *.NarrowPeak
		to *.bed), although the user may want to impose additional filters such as present in
		x number of replicates or CTCF must also overlap a cohesin peak (highly recommended)
		(2) TAD files for downstream filtering, placed in annotation/TADs . If you choose not
		to use TAD filtering this option will be ignored and the pipeline will proceed
		(3) A fasta file for motif searching and scoring (*.fa + *.fai) placed in annotation/motif
		NOTE: for genomes other than hg19 or mm9, you also need to provide a TSS file
		(annotation/TSS) for filtering in addition to the above.
Output: The final merged and filtered loop list is found at 05_filteredloops/Final_Output
		with 3 files: (1) Merged and filtered intra-TAD loops in bed12 format (for UCSC visualization),
		(2) Merged and filtered intra-TAD loops in WashU browser format (for WashU visualization),
		and (3) Anchors for intra-TAD loops (for downstream analyses)
Usage: ./run_pipe.sh <genome build> <fraction of peaks> <loop # target> <tad file used>
		This points to 3 sequential scripts (run_fimo_onemotif.sh, predict_ctcf_chipseq_loops.sh,
		and filter_loops.sh) which can also be run individually as needed (located in bin/).
Dependencies: Bedtools for overlaps, Python (2.7.3 or above) for loop prediction


#For repressive histone mark insulation, please see bin/additional_utilities/JSD_Figure3_DEF
Input:  (1) Mapped reads in BAM format placed in bin/additional_utilities/JSD_Figure3DEF/bam/
	    (2) All peaks/regions you're comparing insulation for placed in the top directory
	    bin/additional_utilities/JSD_Figure3DEF/
Output: JSD insulation scores per bed file input are deposited in the output directory 
		(bin/additional_utilities/JSD_Figure3DEF/output/*_JSD.bed). These represent the 
		original bed file with two appended columns: the first represents the entropy score
		relative to high signal upstream and low signal downstream of the peak, while the 
		second column represents the entropy score relative to low signal upstream and high
		signal upstream (50 0s followed by 50 1s)
Usage:  ./JSD_turbo.sh <BAM_filename>.bam which points to JSD_wrapper.qsub
Dependencies: Bedtools for overlaps and flanking, Samtools for read processing, and Python
		for read processing and calculating JSD. Also uses ROSE utilities, included in the
		directory, for their clever BAM to GFF python scripting. 
 
#For 4C read processing scripts, please see bin/additional_utilities/4C_Figure6
Input:  All gzipped fastq files (*.fastq.gz) placed in bin/additional_utilities/4C_Figure6/input_fastq
Output: Trimmed gzipped fastq files (*.fastq.gz) placed in bin/additional_utilities/4C_Figure6/output_fastq
		These represent all reads that contained the viewpoint specific primer sequence 'TATGGTTAATGATC', which
		is then trimmed, leaving only mappable sequence representing interacting regions 
Usage: ./demultiplex_alb.sh
Dependencies: fastx_trimmer needs to be available as a module or added to the PATH